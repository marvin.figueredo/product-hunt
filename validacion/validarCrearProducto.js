export default function validarCrearCuenta(valores) {

    let errores = {};

    if (!valores.nombre) {
        errores.nombre = "El Nombre es obligatorio";
    }

    if (!valores.empresa) {
        errores.empresa = "La Empresa es un campo obligatorio";
    }

    if (!valores.url) {
        errores.url = "La Url es obligatoria";
    } else if ( !/^(ftp|http|https):\/\/[^ "]+$/.test(valores.url) ) {
        errores.url = "Url inválida";
    }

    if (!valores.descripcion) {
        errores.descripcion = "La Descripción es obligatoria";
    }

    return errores;
}